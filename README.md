Use the following instruction to start the container:
- Pull Dockerfile and config from repository on PC
- Build a Docker image using your parameter IMAGE:TAG
```
docker build . -f DockerFile_NGINX_DEBIAN_9 -t [IMAGE:TAG]
```
- Start the container using your NAME parameter
```
docker run -v $(pwd)/nginx.conf:/usr/local/nginx/conf/nginx.conf -v $(pwd)/mime.types:/usr/local/nginx/conf/mime.types -v $(pwd)/index.html:/index.html --name=[NAME] -itd -p 8001:80 [IMAGE:TAG]
```